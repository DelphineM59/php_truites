<?php
// connexion à la base

require "bdd/bddconfig.php";
session_start();
// recuperation des 3 variables post
$nomok = isset($_POST["nom"]);
$descriptok = isset($_POST["descript"]);
$refcapteurok = isset($_POST["refcapteur"]);

// securisation des variables
if (($nomok) && ($descriptok) && ($refcapteurok)) {
    $nom = strval(htmlspecialchars($_POST["nom"]));
    $descript = strval(htmlspecialchars($_POST["descript"]));
    $refcapteur = intval(htmlspecialchars($_POST["refcapteur"]));

    // insert dans la base
    // requete sql

    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;
    charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $RSLOGIN = $objBdd->prepare("insert INTO bassin (nom,description,refCapteur) VALUES (:nom, :descript, :refcapteur)");
        $RSLOGIN->bindParam(':nom', $nom, PDO::PARAM_STR);
        $RSLOGIN->bindParam(':descript', $descript, PDO::PARAM_STR);
        $RSLOGIN->bindParam(':refcapteur', $refcapteur, PDO::PARAM_STR);
        $RSLOGIN->execute();

        // recupérer la valeur de l'id du nouveau bassin crée
        $lastId = $objBdd->lastInsertId();
    } catch (Exception $prmE) {
        die('Erreur ; ' . $prmE->getMessage());
    }

    // rediriger uniquement vers la page bassin.php
    // header ("Location:http://localhost/truites/bassins.php");
    // remplace par :
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'bassins.php';
    header("Location: http://$serveur$chemin/$page");

} else {
    die('Les paramètres ne sont pas valides');
}    



// rediriger automatiquement vers la page bassins.php
