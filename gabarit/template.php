<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php $titre; ?></title>
        <link rel="stylesheet" href="css/styles.css" />
    </head>
    <p class="login">
<?php
if (isset($_SESSION['logged_in']['login']) == TRUE) {
    //l'internaute est authentifié
    echo $_SESSION['logged_in']['prenom'].' '.$_SESSION['logged_in']['nom'];
    //affichage "Se déconnecter"(logout.php), "Prif", "Paramètres", etc ...
    ?>
    <a href="logout.php">Se déconnecter</a>
</p>
    <?php

} else { 
    //Personne n'est authentifié 
    // affichage d'un lien pour se connecter
    ?>
    <a href="login.php">Connexion</a>
<?php }?>
    <body>
        <div id="conteneur">
            <header>
                <h1>La pisciculture PHP</h1>
            </header>

            <nav>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="bassins.php">Les bassins</a></li>
                    <li><a href="arcenciel.php">La truite arc-en-ciel</a></li>
                    <?php if (isset($_SESSION['logged_in']['login']) == TRUE) {?>
                    <li><a href="ajouterbassin.php">Ajouter un bassin</a></li>
                    <li><a href="supprbassin.php">Supprimer un bassin</a></li>
                    <?php } ?>
                </ul>
            </nav>
            <section>
            <?php echo $contenu; ?>
             </section>

            <footer>
                <p>Copyright TruitesPHP - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>