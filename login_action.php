<?php
require "bdd/bddconfig.php";
//activer les sessions
session_start();

//Tester si les variables POST existent
$paramOK = false;
if (isset($_POST["login"])) {
    $login = strtolower(htmlspecialchars($_POST["login"]));
    if (isset($_POST["password"])) {
        $password = htmlspecialchars($_POST["password"]);
        $paramOK = true;
    }
}

//si login et password sont bien reçus
if ($paramOK == true) {
    // vérifier si le login passord est correct (base de données) : voir après
    // echo $login." - ".$password;
    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;
    charset=utf8", $bddlogin, $bddpass);
        $PDOlistlogins = $objBdd->prepare("SELECT * FROM userweb WHERE login = :login ");
        $PDOlistlogins->bindParam(':login', $login, PDO::PARAM_STR);
        $PDOlistlogins->execute();
    } catch (Exception $prmE) {
        die('Erreur ; ' . $prmE->getMessage());
    }
    //S'il y a un résultat à la requête 
    $row_userweb = $PDOlistlogins->fetch();
    if ($row_userweb != false) {
        //il existe un login identique dans la base
        // vérif du password : voir après
        // rediriger uniquement vers la page bassin.php
        // header ("Location:http://localhost/truites/bassins.php");
        // remplace par :
        $_SESSION['logged_in']['login']=$row_userweb['login'];
        $_SESSION['logged_in']['nom']=$row_userweb['nom'];
        $_SESSION['logged_in']['prenom']=$row_userweb['prenom'];
        $serveur = $_SERVER['HTTP_HOST'];
        $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $page = 'index.php';
        header("Location: http://$serveur$chemin/$page");
    } else {
        //Mauvais login
        $titre = "La pisciculture PHP";
        ob_start(); ?>
        <p><a href="login.php">Se connecter</a></p>
        <p>Authentification incorrecte</p>
        <?php
        $contenu = ob_get_clean();
        session_destroy();
        require 'gabarit/template.php';
        die('Authentification incorrecte');
    }
} else {
    die('Vous devez fournir un login et un mot de passe');
}
$row_userweb->closeCursor(); //libère les ressources de la bdd