<?php
session_start();
$titre = "La temperature";

$idBassin = 0;
$nomBassin = "Bassin inconnu";

$idok = isset($_GET["idBassin"]);
$nomok = isset($_GET["nomBassin"]);

if (($idok) && ($nomok)) {
    $idBassin = intval(htmlspecialchars($_GET["idBassin"]));
    $nomBassin = strval(htmlspecialchars($_GET["nomBassin"]));
}

// requete sql
require "bdd/bddconfig.php";

$idBassin = intval($_GET["idBassin"]);

// requete preparee

$objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;
        charset=utf8", $bddlogin, $bddpass);
$listeTemperature = $objBdd->prepare("select * from temperature where idBassin= :id ORDER by date DESC");
$listeTemperature->execute(array(':id' => $idBassin));
$listeTemperature->bindParam(':id', $idBassin, PDO::PARAM_INT);
$listeTemperature->execute();

// requete non prepare
//    $objBdd= new PDO("mysql:host=$bddserver;
//        dbname=$bddname;
//        charset=utf8",
//    $bddlogin, $bddpass);

 //   $objBdd->setAttribute(PDO::ATTR_ERRMODE,
//     PDO::ERRMODE_EXCEPTION);
//     $listeTemperature = $objBdd->query("SELECT * FROM temperature where idBassin=$idBassin ORDER by date DESC");


ob_start(); ?>

<article>
    <h1>Tempétratures</h1>
    <table>
        <thead>
            <tr>
                <th>Date</th>
                <th>Température (°C)</th>
            </tr>
        </thead>
        <tbody>
            <?php  
            foreach($listeTemperature AS $temp) { ?>
                <tr>
                <td><?php echo $temp['date']; ?></td>
                <td><?php echo $temp['temp']; ?></td>
                </tr>
            <?php }?>
        </tbody>
    </table>
</article>
<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php' ?>