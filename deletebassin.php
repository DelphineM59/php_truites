<?php
require "bdd/bddconfig.php";
session_start();
//Accès seulement si authentifié 
if (isset($_SESSION['logged_in']['login']) !== TRUE) {
    // Redirige vers la page d'accueil (ou login.php) si pas authentifié
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
    $page = 'index.php';
    header("Location: http://$serveur$chemin/$page");
}

$paramOk=false;

// recuperation de la variable post
//$idbassinok = isset($_POST["idbassin"]);

// securisation des variables
if (isset($_POST["idbassin"]))  {
    $idbassin = intval(htmlspecialchars($_POST["idbassin"]));
    $paramOk=true;
}
    // requete sql
    if ($paramOk == true) {
    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;
    charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // SUPPRIMER LES TEMPERATURES DU BASSIN
        $RSLOGIN = $objBdd->prepare("DELETE FROM temperature where idBassin=:idbassin");
        $RSLOGIN->bindParam(':idbassin', $idbassin, PDO::PARAM_INT);
        $RSLOGIN->execute();
        // suppression du bassin de la table bassin
        $RSLOGIN = $objBdd->prepare("DELETE FROM bassin where idBassin=:idbassin");
        $RSLOGIN->bindParam(':idbassin', $idbassin, PDO::PARAM_INT);
        $RSLOGIN->execute();
    } catch (Exception $prmE) {
        die('Erreur ; ' . $prmE->getMessage());
    }
    // rediriger uniquement vers la page bassin.php
    // header ("Location:http://localhost/truites/bassins.php");
    // remplace par :
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'bassins.php';
    header("Location: http://$serveur$chemin/$page");
} else {
    die('Les paramètres ne sont pas valides');
}    

