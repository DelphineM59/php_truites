<?php

$titre = "Les bassins"; 
session_start();

require "bdd/bddconfig.php";
$objBdd= new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);

try{
    $objBdd= new PDO("mysql:host=$bddserver;
        dbname=$bddname;
        charset=utf8",
    $bddlogin, $bddpass);

    $objBdd->setAttribute(PDO::ATTR_ERRMODE,
     PDO::ERRMODE_EXCEPTION);
     $listebassins = $objBdd->query("SELECT * FROM bassin");

} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}

?>

<?php
ob_start(); ?>
                <article>                
                    <h1>Les bassins</h1>
                    <?php
                    while($unbassin = $listebassins->fetch()) {
                        ?>
                        <h2><?= $unbassin['nom']; ?></h2>
                        <p><?= $unbassin['description']; ?></p>
                        <img src="images/<?= $unbassin['photo']; ?>" alt="le bassin">
                        <p><a href="temperatures.php?idBassin=<?= $unbassin['idBassin']; ?>&nomBassin=<?= $unbassin['nom']; ?>">Afficher les températures du <?= $unbassin['nom']; ?></a></p>
                        <?php
                    } //fin du while
                    $listebassins->closeCursor(); //libère les ressources de la bdd ?>
                    <p></p>
                </article>
<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php' ?>


